package com.mthama.itautechnicaltest.model;

import android.util.Log;

import com.mthama.itautechnicaltest.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A model for an account info, where JSON structure is:
 * {
 *     "checkingAccount": <UNS INTEGER>,
 *     "savingsAccount": <UNS INTEGER>,
 *     "contacts": <ARRAY_OF_CONTACTS>
 * }
 */
public class AccountInfoModel {

    public int checkingAccount;
    public int savingsAccount;
    public List<ContactModel> contacts;

    public AccountInfoModel() {
        checkingAccount = 0;
        savingsAccount = 0;
        contacts = new ArrayList<>();
    }

    public AccountInfoModel(int checkingAccount, int savingsAccount, List<ContactModel> contacts) {
        this.checkingAccount = checkingAccount;
        this.savingsAccount = savingsAccount;
        this.contacts = contacts;
    }

    public AccountInfoModel(JSONObject obj) {
        this();
        try {
            JSONObject funds = obj.getJSONObject("funds");
            checkingAccount = funds.getInt("checkingAccount");
            savingsAccount = funds.getInt("savingsAccount");
            JSONArray arr = obj.getJSONArray("contacts");
            for (int i=0; i<arr.length(); i++) {
                ContactModel cm = new ContactModel(arr.getJSONObject(i));
                contacts.add(cm);
            }
            Collections.sort(contacts);
            String index = "-";
            for (int i=0; i<contacts.size(); i++) {
                if (!index.equals(contacts.get(i).contactName.substring(0, 1))) {
                    index = contacts.get(i).contactName.substring(0, 1);
                    ContactModel contactIndex = new ContactModel();
                    contactIndex.contactName = index;
                    contacts.add(i, contactIndex);
                }
            }
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            JSONObject funds = new JSONObject();
            funds.put("checkingAccount", checkingAccount);
            funds.put("savingsAccount", savingsAccount);
            obj.put("funds", funds);
            JSONArray arr = new JSONArray();
            for (int i=0; i<contacts.size(); i++) {
                arr.put(contacts.get(i).toJson());
            }
            obj.put("contacts", arr);
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        return obj;
    }

}
