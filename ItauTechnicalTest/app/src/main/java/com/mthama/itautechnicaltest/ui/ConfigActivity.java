package com.mthama.itautechnicaltest.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.mthama.itautechnicaltest.AbstractActionBarActivity;
import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.device.SharedPref;

public class ConfigActivity extends AbstractActionBarActivity {

    private CheckBox activity_config_usemockchk;
    private EditText activity_config_accountinfoedt;
    private EditText activity_config_posttransferedt;
    private Button activity_config_confirmbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        activity_config_usemockchk = findViewById(R.id.activity_config_usemockchk);
        activity_config_accountinfoedt = findViewById(R.id.activity_config_accountinfoedt);
        activity_config_posttransferedt = findViewById(R.id.activity_config_posttransferedt);
        activity_config_confirmbtn = findViewById(R.id.activity_config_confirmbtn);

        activity_config_usemockchk.setChecked(SharedPref.loadUseLocalMock(ConfigActivity.this));
        activity_config_accountinfoedt.setText(SharedPref.loadGetAccountInfoMockUrl(ConfigActivity.this));
        activity_config_posttransferedt.setText(SharedPref.loadPostTransferMockUrl(ConfigActivity.this));
        activity_config_confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.saveUseLocalMock(ConfigActivity.this, activity_config_usemockchk.isChecked());
                SharedPref.saveGetAccountInfoMockUrl(ConfigActivity.this, activity_config_accountinfoedt.getText().toString());
                SharedPref.savePostTransferMockUrl(ConfigActivity.this, activity_config_posttransferedt.getText().toString());
                finish();
            }
        });
    }

}
