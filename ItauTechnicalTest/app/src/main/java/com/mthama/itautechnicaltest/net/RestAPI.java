package com.mthama.itautechnicaltest.net;

import android.content.Context;
import android.util.Log;

import com.mthama.itautechnicaltest.AppConstants;
import com.mthama.itautechnicaltest.device.SharedPref;
import com.mthama.itautechnicaltest.model.AccountInfoModel;
import com.mthama.itautechnicaltest.model.TransferInputModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;

public class RestAPI {

    private int status							= HttpStatus.SC_OK;
    private InputStream instream				= null;
    private DefaultHttpClient httpClient;
    private HttpGet httpGet						= null;
    private HttpPost httpPost					= null;

    public RestAPI() {
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.ESTABLISHED_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParameters, AppConstants.RESPONSE_TIMEOUT);
        httpClient = new DefaultHttpClient(httpParameters);
    }

    public int getStatus() {
        return status;
    }

    /**
     * This end-point retrieves the contacts and available funds of a given account.
     * Returned JSON format is:
     * {
     *     "funds": {
     *          "checkingAccount": <INTEGER>,
     *          "savingsAccount": <INTEGER>
     *     },
     *     "contacts": <ARRAY_OF_CONTACTS>
     * }
     * where [funds] contains available funds (in centavos) of this given account and [contacts] is
     * a list of strings related to each contact name.
     * @return JSONObject
     */
    public AccountInfoModel getAccountInfo(Context context) {
        AccountInfoModel aim = null;
        try {
            if (SharedPref.loadUseLocalMock(context)) {
                status = HttpStatus.SC_OK;
                return new AccountInfoModel(new JSONObject(
                        getDummyAccountInfoModel()
                ));
            }
            httpGet = new HttpGet("http://www.mocky.io/v2/" + SharedPref.loadGetAccountInfoMockUrl(context));
            httpGet.setHeader("Content-type", "application/json");
            HttpResponse response = httpClient.execute(httpGet);
            if (response != null) {
                status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String inputLine;
                    StringBuilder wsResponse = new StringBuilder();
                    instream = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                    while ((inputLine = reader.readLine()) != null) {
                        wsResponse.append(inputLine);
                    }
                    JSONObject obj = new JSONObject(wsResponse.toString());
                    aim = new AccountInfoModel(obj);
                }
            }
        } catch (ConnectTimeoutException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_REQUEST_TIMEOUT;
        } catch (SocketTimeoutException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_REQUEST_TIMEOUT;
        } catch (final UnsupportedEncodingException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final ClientProtocolException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final JSONException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final IOException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_NOT_FOUND;
        }
        disconnect();
        return aim;
    }

    public boolean postMoneyTransfer(Context context, TransferInputModel input) {
        try {
            if (SharedPref.loadUseLocalMock(context)) {
                status = HttpStatus.SC_OK;
                return new JSONObject(getDummyTransferResponseModel()).getBoolean("transferSuccess");
            }
            HttpPost httpPost = new HttpPost("http://www.mocky.io/v2/" + SharedPref.loadPostTransferMockUrl(context));
            httpPost.setEntity(new UrlEncodedFormEntity(input.getNameValuePairs(), "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    instream = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                    StringBuilder wsResponse = new StringBuilder();
                    String inputLine;
                    while ((inputLine = reader.readLine()) != null) {
                        wsResponse.append(inputLine);
                    }
                    disconnect();
                    return new JSONObject(wsResponse.toString()).getBoolean("transferSuccess");
                }
            }
        } catch (ConnectTimeoutException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_REQUEST_TIMEOUT;
        } catch (SocketTimeoutException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_REQUEST_TIMEOUT;
        } catch (final UnsupportedEncodingException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final ClientProtocolException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final JSONException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        } catch (final IOException e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
            status = HttpStatus.SC_NOT_FOUND;
        }
        disconnect();
        return false;
    }

    // closes the connection
    private void disconnect() {
        try {
            if (httpGet != null) {
                httpGet.abort();
            }
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        try {
            if (httpPost != null) {
                httpPost.abort();
            }
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        try {
            if (instream != null) {
                instream.close();
            }
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        try {
            if (httpClient != null) {
                httpClient.getConnectionManager().shutdown();
            }
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
    }

    private String getDummyAccountInfoModel() {
        return "{" +
            "\"funds\": {" +
                "\"checkingAccount\": 2000000," +
                "\"savingsAccount\": 1000000" +
            "}," +
            "\"contacts\": [" +
                "{" +
                    "\"contactName\": \"Fulano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                    "}," +
                "{" +
                    "\"contactName\": \"Beltrano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Ciclano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Deltrano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Eltrano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Feltrano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Geltrano\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Gertrudes\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Afonso\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}," +
                "{" +
                    "\"contactName\": \"Genivaldo\"," +
                    "\"contactAccountNumber\": \"123456\"," +
                    "\"contactAgencyNumber\": \"654321\"," +
                    "\"contactBank\": \"341\"," +
                    "\"contactDocument\": \"191.191.191-00\"" +
                "}" +
            "]" +
        "}";
    }

    private String getDummyTransferResponseModel() {
        return "{\"transferSuccess\": true}";
    }

}
