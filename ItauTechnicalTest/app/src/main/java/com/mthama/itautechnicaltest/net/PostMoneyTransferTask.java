package com.mthama.itautechnicaltest.net;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.model.AccountInfoModel;
import com.mthama.itautechnicaltest.model.TransferInputModel;
import com.mthama.itautechnicaltest.ui.TransferActivity;

import org.apache.http.HttpStatus;

public class PostMoneyTransferTask extends AsyncTask<Void, Void, Boolean> {

    private ProgressDialog gettingDataDialog;
    private RestAPI client;
    // TODO: resolve this leak point...
    private TransferActivity c;
    private TransferInputModel tim;

    public PostMoneyTransferTask(TransferActivity c, TransferInputModel tim) {
        this.c = c;
        this.tim = tim;
    }

    @Override
    protected void onPreExecute() {
        synchronized (c.lock) {
            c.isLoading = true;
        }
        client = new RestAPI();
        gettingDataDialog = ProgressDialog.show(
                c, c.getText(R.string.label_pleasewait),
                c.getText(R.string.label_postingtransfer),
                true, false
        );
        gettingDataDialog.setIcon(c.getResources().getDrawable(R.mipmap.ic_launcher));
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return client.postMoneyTransfer(c, tim);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        gettingDataDialog.dismiss();
        if (client.getStatus() == HttpStatus.SC_OK) {
            c.setTransferResumePage(result);
            // update local account values if had enough money
            AccountInfoModel m = c.getAccountInfo();
            if (tim.originType.equals("checkingAccount")) {
                m.checkingAccount -= tim.amount;
            } else if (tim.originType.equals("savingsAccount")) {
                m.savingsAccount -= tim.amount;
            }
            c.setAccountInfo(m);
        } else {
            String message = c.getResources().getString(R.string.label_conectionfail);
            String status = ": [HTTP" + client.getStatus() + "]";
            Toast.makeText(c, message + status, Toast.LENGTH_LONG).show();
        }
        synchronized (c.lock) {
            c.isLoading = false;
        }
    }

}
