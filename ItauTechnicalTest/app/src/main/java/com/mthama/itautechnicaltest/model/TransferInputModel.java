package com.mthama.itautechnicaltest.model;

import android.util.Log;

import com.mthama.itautechnicaltest.AppConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A model for an input of a request of transfer, where JSON structure is:
 * {
 *     "destiny": <CONTACT_MODEL>,
 *     "amountOut": <INTEGER>
 * }
 */
public class TransferInputModel {

    public ContactModel destiny;
    public String originType;
    public int amount;

    public TransferInputModel() {
        destiny = new ContactModel();
        originType = "checkingAccount";
        amount = 0;
    }

    public TransferInputModel(ContactModel destiny, String originType, int amount) {
        this.destiny = destiny;
        this.originType = originType;
        this.amount = amount;
    }

    public TransferInputModel(JSONObject obj) {
        this();
        try {
            destiny = new ContactModel(obj.getJSONObject("destiny"));
            originType = obj.getString("originType");
            amount = obj.getInt("amount");
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("destiny", destiny.toJson());
            obj.put("originType", originType);
            obj.put("amount", amount);
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        return obj;
    }

    public List<NameValuePair> getNameValuePairs() {
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("originType", String.valueOf(originType)));
        list.add(new BasicNameValuePair("amount", String.valueOf(amount)));
        list.add(new BasicNameValuePair("contactName", destiny.contactName));
        list.add(new BasicNameValuePair("contactAccountNumber", destiny.contactAccountNumber));
        list.add(new BasicNameValuePair("contactAgencyNumber", destiny.contactAgencyNumber));
        list.add(new BasicNameValuePair("contactBank", destiny.contactBank));
        list.add(new BasicNameValuePair("contactDocument", destiny.contactDocument));
        return list;
    }

}
