package com.mthama.itautechnicaltest.device;

import android.content.Context;
import android.content.SharedPreferences;

import com.mthama.itautechnicaltest.AppConstants;

public class SharedPref {

    public static String loadGetAccountInfoMockUrl(Context context) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        return prefsPrivate.getString(AppConstants.GET_ACCOUNTINFO_MOCK_URL, "");
    }

    public static void saveGetAccountInfoMockUrl(Context context, String url) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsPrivateEditor = prefsPrivate.edit();
        prefsPrivateEditor.putString(AppConstants.GET_ACCOUNTINFO_MOCK_URL, url);
        prefsPrivateEditor.apply();
    }

    public static String loadPostTransferMockUrl(Context context) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        return prefsPrivate.getString(AppConstants.POST_TRANSFER_MOCK_URL, "");
    }

    public static void savePostTransferMockUrl(Context context, String url) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsPrivateEditor = prefsPrivate.edit();
        prefsPrivateEditor.putString(AppConstants.POST_TRANSFER_MOCK_URL, url);
        prefsPrivateEditor.apply();
    }

    public static boolean loadUseLocalMock(Context context) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        return prefsPrivate.getBoolean(AppConstants.USE_LOCAL_MOCK, true);
    }

    public static void saveUseLocalMock(Context context, boolean useLocalMock) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.APP_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsPrivateEditor = prefsPrivate.edit();
        prefsPrivateEditor.putBoolean(AppConstants.USE_LOCAL_MOCK, useLocalMock);
        prefsPrivateEditor.apply();
    }

}

