package com.mthama.itautechnicaltest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.mthama.itautechnicaltest.AbstractActionBarActivity;
import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.model.AccountInfoModel;
import com.mthama.itautechnicaltest.ui.fragment.AmountSelectionFragment;
import com.mthama.itautechnicaltest.ui.fragment.ContactSelectionFragment;
import com.mthama.itautechnicaltest.ui.fragment.TransferResumeFragment;
import com.mthama.itautechnicaltest.net.GetAccountInfoTask;
import com.mthama.itautechnicaltest.ui.view.NonSwipeableViewPager;

public class TransferActivity extends AbstractActionBarActivity {

    public final Object lock = new Object();
    public boolean isLoading = false;

    private AccountInfoModel aim = null;
    private int contactIndex = -1;
    private int accountType = 0;
    private ContactSelectionFragment contactSelectionFragment;
    private AmountSelectionFragment amountSelectionFragment;
    private TransferResumeFragment transferResumeFragment;
    private NonSwipeableViewPager activity_transfer_viewpage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        Toolbar toolbar = findViewById(R.id.activity_transfer_toolbar);
        setSupportActionBar(toolbar);

        contactSelectionFragment = new ContactSelectionFragment();
        amountSelectionFragment = new AmountSelectionFragment();
        transferResumeFragment = new TransferResumeFragment();

        SpannableString s = new SpannableString(getResources().getString(R.string.title_tranfer));
        s.setSpan(
                new ForegroundColorSpan(getResources().getColor(R.color.itauBlue)),
                0,
                getResources().getString(R.string.title_tranfer).length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        getSupportActionBar().setTitle(s);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_prev_b);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // get account information...
        if (!isLoading && aim == null) {
            new GetAccountInfoTask(this).execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_transferopt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (activity_transfer_viewpage != null) {
                    if (activity_transfer_viewpage.getCurrentItem() == 0) {
                        finish();
                    } else if (activity_transfer_viewpage.getCurrentItem() == 1) {
                        activity_transfer_viewpage.setCurrentItem(0, true);
                    } else {
                        activity_transfer_viewpage.setCurrentItem(0, true);
                    }
                }
                break;
            case R.id.action_config:
                Intent newApp = new Intent(TransferActivity.this, ConfigActivity.class);
                newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(newApp);
                break;
        }
        return true;
    }

    public void updatePages() {
        // update the page list with contacts
        activity_transfer_viewpage = findViewById(R.id.activity_transfer_viewpage);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        activity_transfer_viewpage.setAdapter(mPagerAdapter);
    }

    public void setContactSelectionPage() {
        activity_transfer_viewpage.setCurrentItem(0, true);
        if (contactSelectionFragment != null) {
            contactSelectionFragment.updateData();
        }
    }

    public void setAmountSelectionPage() {
        activity_transfer_viewpage.setCurrentItem(1, true);
        if (amountSelectionFragment != null) {
            amountSelectionFragment.updateData();
        }
    }

    public void setTransferResumePage(boolean success) {
        activity_transfer_viewpage.setCurrentItem(2, true);
        if (transferResumeFragment != null) {
            transferResumeFragment.setMessage(success);
        }
    }

    public void setContactIndex(int selectedContactIndex) {
        contactIndex = selectedContactIndex;
    }

    public int getContactIndex() {
        return contactIndex;
    }

    public void setAccountType(int type) {
        accountType = type;
    }

    public int getAccountType() {
        return accountType;
    }

    public AccountInfoModel getAccountInfo() {
        return aim;
    }

    public void setAccountInfo(AccountInfoModel aim) {
        this.aim = aim;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return contactSelectionFragment;
                case 1:
                    return amountSelectionFragment;
                case 2:
                    return transferResumeFragment;
                default:
                    return contactSelectionFragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

    }

}
