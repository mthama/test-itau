package com.mthama.itautechnicaltest.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.ui.TransferActivity;

public class ContactSelectionFragment extends Fragment {

    private TransferActivity context;
    private TextView frag_contactselection_checkingtxt;
    private ImageView frag_contactselection_checkingimg;
    private TextView frag_contactselection_savingstxt;
    private ImageView frag_contactselection_savingsimg;

    @Nullable @Override
    public View onCreateView(
    @NonNull LayoutInflater inflater, @Nullable ViewGroup container,
    @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_contactselection, container, false);
        context = (TransferActivity) getActivity();

        ListView frag_contactselection_listview = rootView.findViewById(R.id.frag_contactselection_listview);
        TextView frag_contactselection_nocontact = rootView.findViewById(R.id.frag_contactselection_nocontact);
        frag_contactselection_checkingtxt = rootView.findViewById(R.id.frag_contactselection_checkingtxt);
        frag_contactselection_checkingimg = rootView.findViewById(R.id.frag_contactselection_checkingimg);
        frag_contactselection_savingstxt = rootView.findViewById(R.id.frag_contactselection_savingstxt);
        frag_contactselection_savingsimg = rootView.findViewById(R.id.frag_contactselection_savingsimg);

        // update checking account value
        updateData();

        frag_contactselection_checkingtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag_contactselection_checkingtxt.setBackgroundResource(R.color.colorVeryLightGray);
                frag_contactselection_checkingimg.setVisibility(View.VISIBLE);
                frag_contactselection_savingstxt.setBackgroundResource(R.color.colorWhite);
                frag_contactselection_savingsimg.setVisibility(View.INVISIBLE);
                context.setAccountType(0);
            }
        });
        frag_contactselection_savingstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag_contactselection_checkingtxt.setBackgroundResource(R.color.colorWhite);
                frag_contactselection_checkingimg.setVisibility(View.INVISIBLE);
                frag_contactselection_savingstxt.setBackgroundResource(R.color.colorVeryLightGray);
                frag_contactselection_savingsimg.setVisibility(View.VISIBLE);
                context.setAccountType(1);
            }
        });

        ContactListAdapter contactListAdapter = new ContactListAdapter();
        frag_contactselection_listview.setAdapter(contactListAdapter);
        frag_contactselection_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                context.setContactIndex(position);
                context.setAmountSelectionPage();
            }
        });

        if (context.getAccountInfo().contacts.size() == 0) {
            frag_contactselection_nocontact.setVisibility(View.VISIBLE);
            frag_contactselection_listview.setVisibility(View.INVISIBLE);
        } else {
            frag_contactselection_nocontact.setVisibility(View.INVISIBLE);
            frag_contactselection_listview.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    public void updateData() {
        String ca = getResources().getString(R.string.label_checkingaccount) + " (" + getResources().getString(R.string.app_currencyid) + " ";
        ca += context.getAccountInfo().checkingAccount/100 + "," + context.getAccountInfo().checkingAccount%100 + ")";
        frag_contactselection_checkingtxt.setText(ca);
        // update savings account value
        String sa = getResources().getString(R.string.label_savingsaccount) + " (" + getResources().getString(R.string.app_currencyid) + " ";
        sa += context.getAccountInfo().savingsAccount/100 + "," + context.getAccountInfo().savingsAccount%100 + ")";
        frag_contactselection_savingstxt.setText(sa);
    }

    private class ContactListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return context.getAccountInfo().contacts.size();
        }

        @Override
        public Object getItem(int position) {
            return context.getAccountInfo().contacts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressWarnings("deprecation") @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final View rowView;
            if (!context.getAccountInfo().contacts.get(position).contactAccountNumber.equals("")) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.row_contact, parent, false);

                TextView row_contactnametxt = rowView.findViewById(R.id.row_contactnametxt);
                String name = context.getAccountInfo().contacts.get(position).contactName;
                String doc = context.getAccountInfo().contacts.get(position).contactDocument;
                String contact = name + " (" + doc + ")";
                row_contactnametxt.setText(contact);

                TextView row_contactinfotxt = rowView.findViewById(R.id.row_contactinfotxt);
                String cc = context.getAccountInfo().contacts.get(position).contactAccountNumber;
                String ag = context.getAccountInfo().contacts.get(position).contactAgencyNumber;
                String bk = context.getAccountInfo().contacts.get(position).contactBank;
                String info = "Cc:" + cc + " | " + "Ag:" + ag + " | " + "Bank:" + bk;
                row_contactinfotxt.setText(info);
            } else {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.row_contactindex, parent, false);

                TextView row_contactindextxt = rowView.findViewById(R.id.row_contactindextxt);
                row_contactindextxt.setText(context.getAccountInfo().contacts.get(position).contactName);

                rowView.setOnClickListener(null);
            }

            return rowView;

        }

    }

}
