package com.mthama.itautechnicaltest.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.ui.TransferActivity;

public class TransferResumeFragment extends Fragment {

    private TransferActivity context;
    private TextView frag_transferresume_messagetxt;

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_transferresume, container, false);
        context = (TransferActivity) getActivity();

        frag_transferresume_messagetxt = rootView.findViewById(R.id.frag_transferresume_messagetxt);
        Button frag_transferresume_okbtn = rootView.findViewById(R.id.frag_transferresume_okbtn);
        frag_transferresume_okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.setContactSelectionPage();
            }
        });

        return rootView;
    }

    public void setMessage(boolean success) {
        if (success) {
            frag_transferresume_messagetxt.setText(getResources().getString(R.string.label_transfersuccess));
        } else {
            frag_transferresume_messagetxt.setText(getResources().getString(R.string.label_transferfailed));
        }
    }

}
