package com.mthama.itautechnicaltest.net;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.net.RestAPI;
import com.mthama.itautechnicaltest.ui.TransferActivity;

import org.apache.http.HttpStatus;

public class GetAccountInfoTask extends AsyncTask<Void, Void, Void> {

    private ProgressDialog gettingDataDialog;
    private RestAPI client;
    // TODO: resolve this leak point...
    private TransferActivity c;

    public GetAccountInfoTask(TransferActivity c) {
        this.c = c;
    }

    @Override
    protected void onPreExecute() {
        synchronized (c.lock) {
            c.isLoading = true;
        }
        client = new RestAPI();
        gettingDataDialog = ProgressDialog.show(
                c, c.getText(R.string.label_pleasewait),
                c.getText(R.string.label_gettinginfo),
                true, false
        );
        gettingDataDialog.setIcon(c.getResources().getDrawable(R.mipmap.ic_launcher));
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        c.setAccountInfo(client.getAccountInfo(c));
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        gettingDataDialog.dismiss();
        if (client.getStatus() == HttpStatus.SC_OK) {
            c.updatePages();
        } else {
            String message = c.getResources().getString(R.string.label_conectionfail);
            String status = ": [HTTP" + client.getStatus() + "]";
            Toast.makeText(c, message + status, Toast.LENGTH_LONG).show();
        }
        synchronized (c.lock) {
            c.isLoading = false;
        }
    }

}
