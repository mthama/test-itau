package com.mthama.itautechnicaltest;

public class AppConstants {

    public static final String APP_TAG = "ItauTechnicalTest";
    public static final String GET_ACCOUNTINFO_MOCK_URL = "GET_ACCOUNTINFO_MOCK_URL";
    public static final String POST_TRANSFER_MOCK_URL = "POST_TRANSFER_MOCK_URL";
    public static final String USE_LOCAL_MOCK = "USE_MOCK";
    public static final int BOOTSPLASH_TIMER = 3000;
    public static final int	ESTABLISHED_TIMEOUT = 60000;
    public static final int	RESPONSE_TIMEOUT = 60000;
    public static final int	TRANSFER_LIMIT = 10000;

}
