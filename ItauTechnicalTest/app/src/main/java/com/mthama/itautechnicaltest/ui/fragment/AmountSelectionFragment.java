package com.mthama.itautechnicaltest.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mthama.itautechnicaltest.AppConstants;
import com.mthama.itautechnicaltest.R;
import com.mthama.itautechnicaltest.model.ContactModel;
import com.mthama.itautechnicaltest.model.TransferInputModel;
import com.mthama.itautechnicaltest.net.PostMoneyTransferTask;
import com.mthama.itautechnicaltest.ui.TransferActivity;

import java.text.NumberFormat;
import java.util.Locale;

public class AmountSelectionFragment extends Fragment {

    private int selectedAmount = 0;
    private TransferActivity context;
    private TextView frag_amountselection_selectedtxt;
    private TextView frag_amountselection_nametxt;
    private TextView frag_amountselection_infotxt;
    private EditText frag_amountselection_amounttxt;
    private TextWatcher moneyFormat;

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_amountselection, container, false);
        context = (TransferActivity) getActivity();

        frag_amountselection_selectedtxt = rootView.findViewById(R.id.frag_amountselection_selectedtxt);
        frag_amountselection_nametxt = rootView.findViewById(R.id.frag_amountselection_nametxt);
        frag_amountselection_infotxt = rootView.findViewById(R.id.frag_amountselection_infotxt);
        frag_amountselection_amounttxt = rootView.findViewById(R.id.frag_amountselection_amounttxt);
        moneyFormat = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                // remove listener to prevent looping
                frag_amountselection_amounttxt.removeTextChangedListener(this);
                // build the actual float value from a string representation
                String strValue = s.toString();
                strValue = strValue.replace(".", "").replace(",", "");
                if (Integer.valueOf(strValue) <= AppConstants.TRANSFER_LIMIT*100) {
                    selectedAmount = Integer.valueOf(strValue);
                } else {
                    selectedAmount = 1000000;
                    NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    String limit = format.format(AppConstants.TRANSFER_LIMIT);
                    limit = getResources().getString(R.string.app_currencyid) + limit.replaceAll("[^\\d,.]", "");
                    String m = getResources().getString(R.string.label_transferlimitmessage) + "\n" + limit;
                    Toast.makeText(context, m, Toast.LENGTH_LONG).show();
                }
                String amountStrRepresentation = selectedAmount / 100 + "," + (selectedAmount % 100) / 10 + "" + (selectedAmount % 100) % 10;
                frag_amountselection_amounttxt.setText(amountStrRepresentation);
                frag_amountselection_amounttxt.setSelection(frag_amountselection_amounttxt.getText().length());
                // add listener again
                frag_amountselection_amounttxt.addTextChangedListener(this);
            }
        };

        frag_amountselection_amounttxt.addTextChangedListener(moneyFormat);

        Button frag_amountselection_onemorebtn = rootView.findViewById(R.id.frag_amountselection_onemorebtn);
        frag_amountselection_onemorebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                int amount = selectedAmount + 100;
                if (amount <= AppConstants.TRANSFER_LIMIT*100) {
                    // remove listener to prevent looping
                    frag_amountselection_amounttxt.removeTextChangedListener(moneyFormat);
                    // update value
                    selectedAmount = amount;
                    String amountStrRepresentation = selectedAmount / 100 + "," + (selectedAmount % 100) / 10 + "" + (selectedAmount % 100) % 10;
                    frag_amountselection_amounttxt.setText(amountStrRepresentation);
                    // add listener again
                    frag_amountselection_amounttxt.addTextChangedListener(moneyFormat);
                } else {
                    String limit = format.format(AppConstants.TRANSFER_LIMIT);
                    limit = getResources().getString(R.string.app_currencyid) + limit.replaceAll("[^\\d,.]", "");
                    String m = getResources().getString(R.string.label_transferlimitmessage) + "\n" + limit;
                    Toast.makeText(context, m, Toast.LENGTH_LONG).show();
                }
            }
        });
        Button frag_amountselection_fivemorebtn = rootView.findViewById(R.id.frag_amountselection_fivemorebtn);
        frag_amountselection_fivemorebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                int amount = selectedAmount + 500;
                if (amount <= AppConstants.TRANSFER_LIMIT*100) {
                    // remove listener to prevent looping
                    frag_amountselection_amounttxt.removeTextChangedListener(moneyFormat);
                    // update value
                    selectedAmount = amount;
                    String amountStrRepresentation = selectedAmount / 100 + "," + (selectedAmount % 100) / 10 + "" + (selectedAmount % 100) % 10;
                    frag_amountselection_amounttxt.setText(amountStrRepresentation);
                    // add listener again
                    frag_amountselection_amounttxt.addTextChangedListener(moneyFormat);
                } else {
                    String limit = format.format(AppConstants.TRANSFER_LIMIT);
                    limit = getResources().getString(R.string.app_currencyid) + limit.replaceAll("[^\\d,.]", "");
                    String m = getResources().getString(R.string.label_transferlimitmessage) + "\n" + limit;
                    Toast.makeText(context, m, Toast.LENGTH_LONG).show();
                }
            }
        });
        Button frag_amountselection_tenmorebtn = rootView.findViewById(R.id.frag_amountselection_tenmorebtn);
        frag_amountselection_tenmorebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                int amount = selectedAmount + 1000;
                if (amount <= AppConstants.TRANSFER_LIMIT*100) {
                    // remove listener to prevent looping
                    frag_amountselection_amounttxt.removeTextChangedListener(moneyFormat);
                    // update value
                    selectedAmount = amount;
                    String amountStrRepresentation = selectedAmount / 100 + "," + (selectedAmount % 100) / 10 + "" + (selectedAmount % 100) % 10;
                    frag_amountselection_amounttxt.setText(amountStrRepresentation);
                    // add listener again
                    frag_amountselection_amounttxt.addTextChangedListener(moneyFormat);
                } else {
                    String limit = format.format(AppConstants.TRANSFER_LIMIT);
                    limit = getResources().getString(R.string.app_currencyid) + limit.replaceAll("[^\\d,.]", "");
                    String m = getResources().getString(R.string.label_transferlimitmessage) + "\n" + limit;
                    Toast.makeText(context, m, Toast.LENGTH_LONG).show();
                }
            }
        });

        Button frag_amountselection_confirmbtn = rootView.findViewById(R.id.frag_amountselection_confirmbtn);
        frag_amountselection_confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferInputModel tim = new TransferInputModel();
                tim.originType = context.getAccountType() == 0 ? "checkingAccount" : "savingsAccount";
                tim.amount = selectedAmount;
                tim.destiny = context.getAccountInfo().contacts.get(context.getContactIndex());
                int availableMoney = 0;
                if (context.getAccountType() == 0) {
                    availableMoney = context.getAccountInfo().checkingAccount;
                } else if (context.getAccountType() == 1) {
                    availableMoney = context.getAccountInfo().savingsAccount;
                }
                if (selectedAmount <= 0) {
                    String message = getResources().getString(R.string.label_selectavalue);
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                } else if (availableMoney < selectedAmount) {
                    String message = context.getResources().getString(R.string.label_notenoughmoney);
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                } else {
                    if (!context.isLoading) {
                        new PostMoneyTransferTask(context, tim).execute();
                    }
                }
            }
        });

        return rootView;
    }

    public void updateData() {

        String selectedAccount = "";
        if (context.getAccountType() == 0) {
            selectedAccount = getResources().getString(R.string.label_checkingaccount) + " (" + getResources().getString(R.string.app_currencyid) + " ";
            selectedAccount += context.getAccountInfo().checkingAccount / 100 + "," + context.getAccountInfo().checkingAccount % 100 + ")";
        } else if (context.getAccountType() == 1) {
            selectedAccount = getResources().getString(R.string.label_savingsaccount) + " (" + getResources().getString(R.string.app_currencyid) + " ";
            selectedAccount += context.getAccountInfo().savingsAccount / 100 + "," + context.getAccountInfo().savingsAccount % 100 + ")";
        }
        frag_amountselection_selectedtxt.setText(selectedAccount);

        ContactModel m = context.getAccountInfo().contacts.get(context.getContactIndex());
        String contact = m.contactName + " (" + m.contactDocument + ")";
        frag_amountselection_nametxt.setText(contact);
        String info = "Cc:" + m.contactAccountNumber + " | " +
                "Ag:" + m.contactAgencyNumber + " | " +
                "Bank:" + m.contactBank;
        frag_amountselection_infotxt.setText(info);

        selectedAmount = 0;
        frag_amountselection_amounttxt.setText("0,00");

    }

}
