package com.mthama.itautechnicaltest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.mthama.itautechnicaltest.ui.TransferActivity;

public class BootsplashActivity extends AbstractActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bootsplash);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Thread bootsplashDelay = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(AppConstants.BOOTSPLASH_TIMER);
                } catch (InterruptedException e) {
                    Log.d(AppConstants.APP_TAG, e.getMessage());
                } finally {
                    // creates a new application instance
                    Intent newApp = new Intent(BootsplashActivity.this, TransferActivity.class);
                    newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(newApp);
                    finish();
                }
            }
        };
        bootsplashDelay.start();
    }

}
