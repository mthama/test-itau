package com.mthama.itautechnicaltest.model;

import android.util.Log;

import com.mthama.itautechnicaltest.AppConstants;

import org.json.JSONObject;

/**
 * A model for a contact, where JSON structure is:
 * {
 *     "accountName": <STRING>,
 *     "contactAccountNumber": <STRING>,
 *     "contactAgencyNumber": <STRING>,
 *     "contactBank": <STRING>,
 *     "contactDocument": <STRING>
 * }
 */
public class ContactModel implements Comparable<ContactModel> {

    public String contactName;
    public String contactAccountNumber;
    public String contactAgencyNumber;
    public String contactBank;
    public String contactDocument;

    public ContactModel() {
        contactName = "";
        contactAccountNumber = "";
        contactAgencyNumber = "";
        contactBank = "";
        contactDocument = "";
    }

    public ContactModel(String contactName, String contactAccountNumber,
    String contactAgencyNumber, String contactBank, String contactDocument) {
        this.contactName = contactName;
        this.contactAccountNumber = contactAccountNumber;
        this.contactAgencyNumber = contactAgencyNumber;
        this.contactBank = contactBank;
        this.contactDocument = contactDocument;
    }

    public ContactModel(JSONObject obj) {
        this();
        try {
            contactName             = obj.getString("contactName");
            contactAccountNumber    = obj.getString("contactAccountNumber");
            contactAgencyNumber     = obj.getString("contactAgencyNumber");
            contactBank             = obj.getString("contactBank");
            contactDocument         = obj.getString("contactDocument");
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("contactName", contactName);
            obj.put("contactAccountNumber", contactAccountNumber);
            obj.put("contactAgencyNumber", contactAgencyNumber);
            obj.put("contactBank", contactBank);
            obj.put("contactDocument", contactDocument);
        } catch (Exception e) {
            Log.d(AppConstants.APP_TAG, e.getMessage());
        }
        return obj;
    }

    @Override
    public int compareTo(ContactModel f) {
        return contactName.compareTo(f.contactName);
    }

}
