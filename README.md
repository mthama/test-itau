# A Technical Test for ITAÚ

Development of a simple Money-Transfer app. Chosen platform and language: **Android/Java**.

## Project Description
### Itaú - Exercício Mobile iOS
#### Regras gerais
- Você tem uma semana para executar o exercício;
- Você deve desenvolver o projeto na plataforma que deseja trabalhar (iOS ou Android) e pode escolher a linguagem que tem maior domínio na plataforma;
- O projeto precisa ser desenvolvido em linguagem nativa;
- Quando terminar, compartilhe em um repositório privado (sugestão: Bitbucket);
- Você pode usar bibliotecas externas para facilitar seu trabalho;
- A comunicação deve ser realizada chamando APIs.
#### Visão Geral
Como um banco, precisamos garantir que nossos clientes consigam transferir quantias entre contas. O desafio deste exercício é criar um fluxo simples onde isto seja possível.
O objetivo desta implementação é simples, e como um exercício, não vamos tratar todos os casos de uso possíveis.
Crie um modelo de JSON que faça sentido para construção da tela.
Você pode usar o www.mocky.io para criar estes modelos e utilizar no app.
#### Exercício
Observe as telas: [1.1-Screen 1](https://bytebucket.org/mthama/testeitau/raw/b2004f76706421c0fa8093e73635c8e82b9b408f/Description/1.1-Screen%201.png?token=dacd318f56775ffa80b068c6479ac3811dbaaa93) e [2.1-Screen 2](https://bytebucket.org/mthama/testeitau/raw/b2004f76706421c0fa8093e73635c8e82b9b408f/Description/2.1-Screen%202.png?token=398e1cccec601ad72b71e9beaf735e2b157e598e).
Nesta primeira parte o cliente irá selecionar entre uma conta corrente e conta poupança para debito do valor a ser transferido.
Você pode colocar quantos contatos quiser, mas eles precisam estar ordenados alfabeticamente.
Para transferir, o cliente sempre precisa selecionar uma conta origem e um contato.
Ao selecionar o contato, o cliente deve ser direcionado para a segunda tela, onde ele vai informar o valor e confirmar a transferência.
Ele só pode transferir valores até 10.000,00.
É importante informarmos para o cliente sempre que uma requisição de dados estiver sendo executada.

## JSON Structures
There are two web services, one to GET account information (available money and list of contacts) and other to POST a transfer.
### GET ACCOUNT_INFO
- checkingAccount: <UNS INTEGER>
- savingsAccount: <UNS INTEGER>
- contactName: <STRING>
- contactAccountNumber: <STRING>
- contactAgencyNumber: <STRING>
- contactBank: <STRING>
- contactDocument: <STRING>
```
RESPONSE: {
    "funds": {
        "checkingAccount": 2000000,
        "savingsAccount": 1000000
    },
    "contacts": [
        {
            "contactName": "Fulano”,
            "contactAccountNumber": "123456",
            "contactAgencyNumber": "654321",
            "contactBank": "341",
            "contactDocument": "19119119100"
        },
        . . .
    ]
}
```
### POST TRANSFER
- originType: <STRING> ("checkingAccount" | "savingsAccount")
- amount: <UNS INTEGER> (less than 1000000 cents)
- contactName: <STRING>
- contactAccountNumber: <STRING>
- contactAgencyNumber: <STRING>
- contactBank: <STRING>
- contactDocument: <STRING>
- transferSuccess: <BOOLEAN>
```
REQUEST: {
    "originType": "checkingAccount",
    "amount": 1000000,
    "destiny": {
        "contactName": "Fulano”,
        "contactAccountNumber": "123456",
        "contactAgencyNumber": "654321",
        "contactBank": "341",
        "contactDocument": "19119119100"
    }
}
RESPONSE: {
    "transferSuccess": <BOOLEAN>
}
```

## KeyStore Information
- KeyStore Password: itautechnicaltest
- Key Alias: itautechnicaltest
- Key Password: itautechnicaltest

## Project Screenshots
- [contact select](https://bytebucket.org/mthama/testeitau/raw/d30152d745bed4fd89d98c15df455f9b31086d66/Description/contact_select.jpg?token=d4ac3ecf72672c4c9b44a5fa1912e1b43653c3da)
- [amount select](https://bytebucket.org/mthama/testeitau/raw/d30152d745bed4fd89d98c15df455f9b31086d66/Description/amount_select.jpg?token=9af0d46c6f1e55d3c5c3acb2111bc8e2ca47deaa)
- [config](https://bytebucket.org/mthama/testeitau/raw/d30152d745bed4fd89d98c15df455f9b31086d66/Description/mock_config.jpg?token=c9f97ce20211dd9b7208aca75b5b9dd88d2ef0b2)
- [resume](https://bytebucket.org/mthama/testeitau/raw/d30152d745bed4fd89d98c15df455f9b31086d66/Description/resume.jpg?token=ffa95660c2a4ca51ec6593b9945ec7002e2d061d)

## Project Backlog
### Day 1 (Friday 13/04/2018)
- Environment update and instalations
- Project structures;
- Bootsplash view;
- Main navigation;
- Rest client.
### Day 2 (Saturday 14/04/2018)
- Data models;
- Fragment of account and contact selection;
- Design and iconset;
- Keystore creation;
- Readme.
### Day 3 (Sunday 15/04/2018)
- Fragment of transfer;
- Fragment of transfer resume;
- Mock URL configurable.
### Day 4 (Monday 16/04/2018)
- Tests in real device.

## ToDo and "should be nice to have"
- Reduce verbosity in some snippets and classes;
- Check and resolve some leak points;
- Profile image in contact list;
- Support to multiple resolutions;
- Code ofuscation;
- Multilanguage;
- Test suit.